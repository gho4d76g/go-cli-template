package main

import (
	"errors"
	"flag"
)

type SubCommand struct {
	logger *AppLogger
}

// Synopsis satisfied michellh/chi
func (c *SubCommand) Synopsis() string {
	return "write General Content of this command in 50~60 characters"
}

// Help satisfied micchell/chi
func (c *SubCommand) Help() string {
	return "write Usage of this command"
}

// Run satisfied micchell/chi
func (c *SubCommand) Run(args []string) int {

	// parse subcommand flags
	var fLabel string
	flags := flag.NewFlagSet("subcommand", flag.ContinueOnError)
	flags.StringVar(&fLabel, "label", "-", "add output data label")
	flags.StringVar(&fLabel, "l", "-", "add output data label")
	if err := flags.Parse(args); err != nil {
		c.logger.FatalLog(err)
		return ExitCodeError
	}

	// print
	c.logger.Log("--labal option: %s", fLabel)

	// write error
	c.logger.FatalLog(errors.New("test"))

	// write debug
	c.logger.Debug("--labal option: %s", fLabel)

	return ExitCodeOK
}
