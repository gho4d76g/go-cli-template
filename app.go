package main

import (
	"fmt"
	"io"
	"os"

	"github.com/Sirupsen/logrus"
	colorable "github.com/mattn/go-colorable"
)

// App
type App struct {
	name    string
	version string
	logger  *AppLogger
}

type AppLogger struct {
	debug bool
	std   *logrus.Logger
	err   *logrus.Logger
}

// NewApp return *App
func NewApp() *App {
	var debug bool
	envDebug := os.Getenv("DEBUG")
	if envDebug == "true" {
		debug = true
	}
	// init logger
	return &App{
		name:    "go-cli-template",
		version: "0.0.1",
		logger: &AppLogger{
			debug: debug,
			std:   InitLogger(colorable.NewColorableStdout()),
			err:   InitLogger(os.Stderr),
		},
	}
}

//
func InitLogger(w io.Writer) *logrus.Logger {
	var log = logrus.New()
	log.Formatter = new(logrus.TextFormatter)
	log.Formatter.(*logrus.TextFormatter).DisableTimestamp = true // remove timestamp
	log.Out = w
	return log
}

// FatalLog print error log into App.errStream
func (applogger *AppLogger) FatalLog(err error) {
	applogger.err.Error(err)
}

// Log print error log into App.errStream
func (applogger *AppLogger) Log(format string, a ...interface{}) {
	fmt.Fprintf(os.Stdout, format, a...)
}

// debugg print debaug log into App.outStreme
func (applogger *AppLogger) Debug(format string, a ...interface{}) {
	if applogger.debug {
		applogger.std.Debug(fmt.Sprintf(format, a...))
	}
}
