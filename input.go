package main

import (
	"bufio"
	"errors"
	"os"
	"strconv"
)

//
func loadFloat64DataSeries() ([]float64, error) {
	xs := []float64{}
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		f64, err := strconv.ParseFloat(s.Text(), 64)
		if err != nil {
			return xs, err
		}
		xs = append(xs, f64)
	}
	if len(xs) < 2 {
		return xs, errors.New("At least two elements are required for the data series")
	}
	return xs, nil
}
