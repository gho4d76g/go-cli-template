go-cli-template
---

golangでコマンドラインツールを作るときのテンプレート

## dependency

* [github.com/mitchellh/cli](https://github.com/mitchellh/cli)
* [github.com/Sirupsen/logrus](https://github.com/Sirupsen/logrus)
* [github.com/mattn/go-colorable](https://github.com/mattn/go-colorable)

## usage

~~~
$ go get gitlab.point-service.jp/shu_yamada/go-cli-template
~~~

## build

### with cMake

~~~
$ make deps
~~~

~~~
$ make
~~~

~~~
$ make install
~~~

### without cMake

Install [glide](http://github.com/Masterminds/glide)

~~~
$ go get github.com/Masterminds/glide
$ go install github.com/Masterminds/glide
~~~

Install dependencies to ~/vendor directory.
~~~
$ glide install
~~~

Build package

+ for Windows(32-bit) platform
~~~
$ GOOS=windows GOARCH=386 go build -o ${binaly}_dawrin_396.exe
~~~

+ for Windows(64-bit) platform
~~~
$ GOOS=windows GOARCH=amd64 go build -o ${binaly}_windows_amd64.exe
~~~

+ for OSX(64-bit) platform
~~~
$ GOOS=darwin GOARCH=amd64 go build -o ${binaly}_dawrin_amd64
~~~

+ for Linux(64-bit) platform
~~~
$ GOOS=linux GOARCH=amd64 go build -o -o ${binaly}_linux_amd64
~~~
